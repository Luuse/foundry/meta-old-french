# Meta-Old-French
This is __the first !__ font created with [Plancton](https://gitlab.com/Luuse/Luuse.tools/plancton-editor).

Ce travail est une évolution et adaptation du dessin de la [Hersey-Old-French](http://hershey-noailles.luuse.io/www/#old-french) créé par Luuse en 2018.




![test](docs/test.jpg)

## Inspiration

![old-french](docs/01.png)

![old-french](docs/02.png)

![old-french](docs/03.png)

![old-french](docs/03.png)

![old-french](docs/04.png)

